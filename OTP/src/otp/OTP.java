package otp;
import java.security.SecureRandom;
import java.io.*;
import java.time.Instant;

/**
 * Version: 1
 * Date Created:
 * Author: Jack Warner-Pankhurst, University of East Anglia, 100199337
 * Description:
 */
public class OTP implements Runnable
{
    private int randomNum;
    private final Instant endTime = Instant.now().plusSeconds(30);
    
    public OTP(){}
    
    @Override
    public void run()
    {
        OTP o = new OTP();
        SecureRandom r = new SecureRandom();
        int num = Math.abs(r.nextInt(100000000));
        o.setRandomNum(num);
        
        String s = Integer.toString(num);
        
        
    }
    
    public void setRandomNum(int i)
    {
        this.randomNum = i;
    }
    
    public Instant getEndTime()
    {
        return this.endTime;
    }
    
    public static void main(String[] args) 
    {


    }
}