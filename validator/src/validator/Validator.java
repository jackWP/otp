package validator;
import java.io.*;
import java.nio.file.Files;
import java.time.Instant;
import java.util.Scanner;

/**
 * Version: 1
 * Date Created:
 * Author: Jack Warner-Pankhurst, University of East Anglia, 100199337
 * Description:
 */
public class Validator 
{
    
    public static void main(String[] args) 
    {
        try
        {
            BufferedReader r = new BufferedReader(new InputStreamReader(System.in)); //create input reader
            
            System.out.println("Would you like to log in as an existing user y/n? (If no user fiile is found you will be asked to create login details regardless)");
            File f = new File("user.txt");
            String c = r.readLine(); //ask user to either create a new account or login with a new one
            
            
            if("n".equals(c) || !f.exists()) //if the user requests to login as a new user, or if no account exists
            {
                System.out.println("Creating new user details...");
                System.out.println("Please enter your desired username:");
                String username = r.readLine();
                System.out.println("Please enter your desired password:");
                String password = r.readLine();
                System.out.println("Saving credentials..."); //take user input and save as username and password
                
                BufferedWriter writer = new BufferedWriter(new FileWriter("user.txt"));
                writer.write(username + "\n" + password);
                writer.close(); //write user credientials to file                
            }
            
            String providedUsername  = "";
            String providedPassword = "";
            
            f = new File("user.txt");
            Scanner reader = new Scanner(f); //open user credential file
            
            String username = reader.nextLine();
            String password = reader.nextLine(); //read stored user credentials
            
            System.out.println("Please enter you credentials, or type exit to exit");
            while(!username.equals(providedUsername) || !password.equals(providedPassword)) //loop while user credintials are incorrect
            {
                System.out.println("Please enter your username");
                providedUsername = r.readLine();
                
                if(providedUsername.equals("exit")) //if user types exit, exit the program
                {
                    System.out.println("Exiting...");
                    return;
                }
                
                System.out.println("Please enter your password");
                providedPassword = r.readLine();
            }
                      
            OTP otpThread = new OTP();
            Thread t = new Thread(otpThread);   //---- Start OTP thread
            t.start();
            
            while(t.getState() != Thread.State.TERMINATED){} //wait for thread
            
            int userOTP = 0;           
            boolean isNumeric = false;
            
            while(!isNumeric)// while the inputted string is not numeric only
            {
                try //try to parse the input string to an int
                {
                    System.out.println("Please enter the shown one-time password within 20 seconds.");
                    userOTP = Integer.parseInt(r.readLine());
                    isNumeric = true; //on successful parse, set bool to true and exit loop
                }
                catch(NumberFormatException e) //if it fails (due to charcters being present) catch exception
                {
                    System.out.println("Please input only numbers (8 digits)"); //print error then loop
                }
            }
            
            f = new File("otp.file"); //open otp file
            
            byte[] fileBytes = Files.readAllBytes(f.toPath()); //read bytes from file
            
            String fileString = new String(fileBytes);
            String[] strings = fileString.split("\n"); //Cconvert bytes to string and split into OTP and timestamp
            
            int otp = Integer.parseInt(strings[0]); //get otp
            Instant endTime = Instant.parse(strings[1]); //get timestamp
            
            Instant currentTime = Instant.now(); //get current time to see if time has expired
            
            if(endTime.compareTo(currentTime) <=0)
            {
                System.out.println("OTP expired, login failed.");
                System.out.println("Deleting old OTP file...");
                f.delete();
            }
            else if(userOTP != otp)
            {
                System.out.println("OTP incorrect, login failed.");
                System.out.println("Deleting old OTP file...");
                f.delete();
            }
            else
            {
                System.out.println("Correct credentials and OTP, you are logged in!");
                System.out.println("Deleting old OTP file...");
                f.delete();
            }
            
            System.out.println("Press enter to exit.");
            System.in.read();
            
            reader.close();
            
        }
        catch(IOException | NumberFormatException e)
        {
            System.out.println(e);
        } //incase otp/timestamp parse incorrectly
    }
}