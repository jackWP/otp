package validator;
import java.security.SecureRandom;
import java.io.*;
import java.time.Instant;

/**
 * Version: 1
 * Date Created:
 * Author: Jack Warner-Pankhurst, University of East Anglia, 100199337
 * Description:
 */
public class OTP implements Runnable
{   
    @Override
    public void run()
    {
        SecureRandom r = new SecureRandom(); //get new instance of secure random
        int num = Math.abs(r.nextInt(100000000)); //create secure 8 digit number

        String OTPString = Integer.toString(num); //parse to string
        Instant endTime = Instant.now().plusSeconds(20); //parse current time + 20 seconds to string

        File file = new File("otp.file"); //crate file

        try(OutputStream os = new FileOutputStream(file)) //write bytes to file then close
        {
            String s = OTPString + "\n" + endTime.toString();
            os.write(s.getBytes());
        }
        catch(IOException e)
        {
            System.out.println(e);
        }

        System.out.println(num); //print opt to console
    }
}